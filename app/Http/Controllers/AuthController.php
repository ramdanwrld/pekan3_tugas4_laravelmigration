<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('page.form');
    }

    public function welcome(Request $request){
        $first_nama = $request['first_nama'];
        $last_nama = $request['last_nama'];
        return view('page.welcome', compact('first_nama','last_nama'));
    }
}
